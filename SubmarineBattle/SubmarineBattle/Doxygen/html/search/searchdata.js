var indexSectionsWithContent =
{
  0: "abcdefgilmnoprstx",
  1: "abdefgmprs",
  2: "sx",
  3: "acfgimos",
  4: "git",
  5: "mp",
  6: "bdgilmnopst",
  7: "mp"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "properties",
  7: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Properties",
  7: "Events"
};

