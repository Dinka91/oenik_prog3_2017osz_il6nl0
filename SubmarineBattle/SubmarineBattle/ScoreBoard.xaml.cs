﻿namespace SubmarineBattle
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// ScoreBoard.xaml codebehind fájl
    /// </summary>
    public partial class ScoreBoard : Window
    {
        private ScoreModel scoreModel;

        public ScoreBoard(ScoreModel scoreModel)
        {
            this.InitializeComponent();
            this.scoreModel = scoreModel;
            this.FillData();
        }

        private void FillData()
        {
             foreach (KeyValuePair<string, TimeSpan> item in this.scoreModel.ScoreDict)
             {
                 Label l = new Label();
                 l.Content = item.Key + ": " + item.Value;
                 l.FontSize = 15;
                 l.FontWeight = FontWeights.Bold;
                 this.screen.Children.Add(l);
             }
        }
    }
}
