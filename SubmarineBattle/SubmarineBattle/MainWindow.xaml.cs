﻿namespace SubmarineBattle
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using System.Windows.Threading;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private BusinessLogic bL;
        private MoveEnum playerA;
        private MoveEnum playerB;
        private bool playerAMine;
        private bool playerBMine;
        private DispatcherTimer dt;
        private DispatcherTimer mineTicker;
        private Stopwatch sw;
        private int counter;

        public MainWindow()
        {
            try
            {
                this.InitializeComponent();
                this.dt = new DispatcherTimer();
                this.counter = 0;
                this.dt.Interval = TimeSpan.FromMilliseconds(200);
                this.dt.Tick += this.Dt_Tick;
                this.mineTicker = new DispatcherTimer();
                this.mineTicker.Interval = TimeSpan.FromSeconds(1);
                this.mineTicker.Tick += this.MineTicker_Tick;
                this.sw = new Stopwatch();
                this.NewGame(null, null);
                new SettingWindow(this.bL.Modell).ShowDialog();
                this.NewGame(this.bL.Modell.PlayerA, this.bL.Modell.PlayerB);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, string.Empty, MessageBoxButton.OK, MessageBoxImage.Error);
                this.Close();
            }
        }

        private void NewGame(PlayerModel oldPlayerA, PlayerModel oldPlayerB)
        {
            this.playerA = MoveEnum.Stay;
            this.playerB = MoveEnum.Stay;
            this.playerAMine = false;
            this.playerBMine = false;
            if (this.bL != null && this.bL.Modell != null)
            {
                this.bL = new BusinessLogic(this.bL.Modell.Level);
            }
            else
            {
                this.bL = new BusinessLogic(MapEnum.Lvl01);
            }

            if (oldPlayerA != null)
            {
                this.bL.Modell.PlayerA.Name = oldPlayerA.Name;
                this.bL.Modell.PlayerA.Type = oldPlayerA.Type;
            }

            if (oldPlayerB != null)
            {
                this.bL.Modell.PlayerB.Name = oldPlayerB.Name;
                this.bL.Modell.PlayerB.Type = oldPlayerB.Type;
            }

            this.DataContext = this.bL.Modell;
            this.bL.MinePlantedEvent += this.BL_MinePlantedEvent;

            this.dt.Start();

            this.mineTicker.Start();

            this.sw.Restart();
        }

        private void MineTicker_Tick(object sender, EventArgs e)
        {
            string winnerName = this.bL.MineTimerChecker();
            this.stopper.Header = this.sw.Elapsed.ToString("mm\\:ss");
            if (winnerName != null)
            {
                this.mineTicker.Stop();
                this.dt.Stop();
                this.sw.Stop();
                this.bL.AddNewResult(winnerName, this.sw.Elapsed);
                bool? d = new GameOverWindow(winnerName).ShowDialog();
                if (d != null && d.Value)
                {
                    this.sw.Restart();
                    this.NewGame(this.bL.Modell.PlayerA, this.bL.Modell.PlayerB);
                }
                else
                {
                    this.Close();
                }
            }
        }

        private void BL_MinePlantedEvent(object sender, EventArgs e)
        {
            this.playerAMine = false;
            this.playerBMine = false;
        }

        private void Dt_Tick(object sender, EventArgs e)
        {
            this.counter++;
            if (this.counter == 5)
            {
                this.counter = 0;
            }

            this.bL.Move(this.playerA, this.playerB, this.playerAMine, this.playerBMine, this.counter);
            this.playerA = MoveEnum.Stay;
            this.playerB = MoveEnum.Stay;
        }

        private void MenuItem_NewGame_Click(object sender, RoutedEventArgs e)
        {
            this.NewGame(this.bL.Modell.PlayerA, this.bL.Modell.PlayerB);
        }

        private void MenuItem_Scoreboard_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            new ScoreBoard(this.bL.ScoreModel).ShowDialog();
            this.NewGame(this.bL.Modell.PlayerA, this.bL.Modell.PlayerB);
            this.Show();
        }

        private void MenuItem_Exit_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult r = MessageBox.Show("Biztos ki akarsz lépni?", string.Empty, MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (r == MessageBoxResult.Yes)
            {
                this.Close();
            }
        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.Space))
            {
                this.playerAMine = true;
            }

            if (Keyboard.IsKeyDown(Key.NumPad0))
            {
                this.playerBMine = true;
            }

            if (Keyboard.IsKeyDown(Key.S))
            {
                this.playerA = MoveEnum.Down;
            }
            else if (Keyboard.IsKeyDown(Key.W))
            {
                this.playerA = MoveEnum.Up;
            }
            else if (Keyboard.IsKeyDown(Key.A))
            {
                this.playerA = MoveEnum.Left;
            }
            else if (Keyboard.IsKeyDown(Key.D))
            {
                this.playerA = MoveEnum.Rigth;
            }
            else
            {
                this.playerA = MoveEnum.Stay;
            }

            if (Keyboard.IsKeyDown(Key.Down))
            {
                this.playerB = MoveEnum.Down;
            }
            else if (Keyboard.IsKeyDown(Key.Up))
            {
                this.playerB = MoveEnum.Up;
            }
            else if (Keyboard.IsKeyDown(Key.Left))
            {
                this.playerB = MoveEnum.Left;
            }
            else if (Keyboard.IsKeyDown(Key.Right))
            {
                this.playerB = MoveEnum.Rigth;
            }
            else
            {
                this.playerB = MoveEnum.Stay;
            }
        }

        private void MenuItem_Setting_Click(object sender, RoutedEventArgs e)
        {
            this.sw.Stop();
            this.mineTicker.Stop();
            this.dt.Stop();
           if (new SettingWindow(this.bL.Modell).ShowDialog() == true)
            {
                this.sw.Restart();
                this.NewGame(this.bL.Modell.PlayerA, this.bL.Modell.PlayerB);
            }

            this.sw.Start();
            this.mineTicker.Start();
            this.dt.Start();
        }

        private void MenuItem_Rules_Click(object sender, RoutedEventArgs e)
        {
            this.sw.Stop();
            this.mineTicker.Stop();
            this.dt.Stop();
            new Rules().ShowDialog();
            this.sw.Start();
            this.mineTicker.Start();
            this.dt.Start();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            bL.FileLogger();
        }
    }
}
