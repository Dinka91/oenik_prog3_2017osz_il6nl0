﻿namespace SubmarineBattle
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Üres pályaelem
    /// </summary>
    public class EmptyTile
    {
        /// <summary>
        /// Gets hátteret
        /// </summary>
        public virtual Brush GetBackGround => Brushes.Transparent;

        /// <summary>
        /// Gets a value indicating whether rá lehet-e lépni
        /// </summary>
        public virtual bool IsBlocked => false;

        protected Brush GetBrush(string filename) // itt használjuk csak
        {
            ImageBrush ib = new ImageBrush(
                new BitmapImage(
                    new Uri(BusinessLogic.GetPath() + filename, UriKind.Absolute)));

            // csempés rajzoláshoz
            ib.TileMode = TileMode.Tile;
            ib.Viewport = new Rect(0, 0, MapModel.TILEW, MapModel.TILEH);
            ib.ViewportUnits = BrushMappingMode.Absolute;
            return ib;
        }
    }
}
