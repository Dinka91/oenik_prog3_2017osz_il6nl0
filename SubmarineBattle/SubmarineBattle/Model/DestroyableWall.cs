﻿namespace SubmarineBattle
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Rombolható fal pályaelem
    /// </summary>
    public class DestroyableWall : EmptyTile
    {
        /// <summary>
        /// Gets  hátteret
        /// </summary>
        public override Brush GetBackGround
        {
            get
            {
                return this.GetBrush("destroyablewall.jpg");
            }
        }

        /// <summary>
        /// Gets a value indicating whether rá lehet-e lépni
        /// </summary>
        public override bool IsBlocked
        {
            get
            {
                return true;
            }
        }
    }
}
