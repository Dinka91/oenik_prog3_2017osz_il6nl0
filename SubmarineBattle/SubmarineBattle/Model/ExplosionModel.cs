﻿namespace SubmarineBattle
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Robbanás pályaelem
    /// </summary>
    public class ExplosionModel : EmptyTile
    {
        public ExplosionModel(PlayerModel m)
        {
            this.Owner = m;
            this.Timer = 2;
        }

        /// <summary>
        /// Gets a value indicating whether rá lehet-e lépni
        /// </summary>
        public override bool IsBlocked => true;

        /// <summary>
        /// Gets hátteret
        /// </summary>
        public override Brush GetBackGround => this.GetBrush("exp.png");

        /// <summary>
        /// Gets or sets a robbanás tulajdonosát
        /// </summary>
        public PlayerModel Owner { get; set; }

        /// <summary>
        /// Gets or sets Visszaszámlálás értéke a robbanás feloszlásáig
        /// </summary>
        public int Timer { get; set; }
    }
}
