﻿namespace SubmarineBattle
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Tengeralattjáró típusokat tartalamzó enum
    /// </summary>
    public enum PlayerTypeEnum
    {
        Slow, Normal, Fast
    }

    /// <summary>
    /// Irányokat tartalmazó enum
    /// </summary>
    public enum MoveEnum
    {
        Stay, Up, Down, Left, Rigth
    }

    /// <summary>
    /// Játékos pályaelem
    /// </summary>
    public class PlayerModel : EmptyTile
    {
        private PlayerTypeEnum type;
        private bool isPoweredUp;
        private string name;
        private int bombCount;

        public PlayerModel()
        {
            this.Type = PlayerTypeEnum.Normal;
            this.isPoweredUp = false;
            this.Name = "Default Dezső";
            this.BombCount = 0;
        }

        /// <summary>
        /// Gets a value indicating whether rá lehet-e lépni a mezőre
        /// </summary>
        public override bool IsBlocked => true;

        /// <summary>
        /// Gets or sets tengeralattjáró típusát
        /// </summary>
        public PlayerTypeEnum Type { get => this.type;  set => this.type = value; }

        /// <summary>
        /// Gets or sets a value indicating whether vett-e fel fejlesztést
        /// </summary>
        public bool IsPoweredUp { get => this.isPoweredUp; set => this.isPoweredUp = value; }

        /// <summary>
        /// Gets or sets játékos nevét
        /// </summary>
        public string Name { get => this.name;  set => this.name = value; }

        /// <summary>
        /// Gets or sets játékos áltak lerakott bombát számát
        /// </summary>
        public int BombCount { get => this.bombCount; set => this.bombCount = value; }

        /// <summary>
        /// Gets hátteret
        /// </summary>
        public override Brush GetBackGround
        {
            get
            {
                switch (this.type)
                {
                    case PlayerTypeEnum.Slow: return this.GetBrush("slow.png");

                    case PlayerTypeEnum.Normal: return this.GetBrush("norm.png");

                    case PlayerTypeEnum.Fast: return this.GetBrush("fast.png");

                    default: return this.GetBrush("norm.png");
                }
            }
        }
    }
}
