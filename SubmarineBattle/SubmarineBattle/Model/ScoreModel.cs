﻿namespace SubmarineBattle
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Eremdénytároló modell
    /// </summary>
    public class ScoreModel
    {
        private List<KeyValuePair<string, TimeSpan>> score;

        public ScoreModel()
        {
            string path = BusinessLogic.GetPath() + "Scoreboard.txt";
            this.ScoreDict = new List<KeyValuePair<string, TimeSpan>>();
            if (File.Exists(path))
            {
                string[] s = File.ReadAllLines(path);
                foreach (string item in s)
                {
                    string[] v = item.Split('\t');

                    this.score.Add(new KeyValuePair<string, TimeSpan>(v[0], TimeSpan.Parse("00:" + v[1])));
                }
            }
            else
            {
                File.Create(path);
            }
        }

        /// <summary>
        /// Gets or sets eredményeket tartalmazó Dictionaryt
        /// </summary>
        public List<KeyValuePair<string, TimeSpan>> ScoreDict
        {
            get => this.score;
            set => this.score = value;
        }
    }
}
