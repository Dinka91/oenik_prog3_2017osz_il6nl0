﻿namespace SubmarineBattle
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Nem rombolható fal pályaelem
    /// </summary>
    public class FixWall : EmptyTile
    {
        /// <summary>
        /// Gets hátteret
        /// </summary>
        public override Brush GetBackGround => this.GetBrush("fixwall.jpg");

        /// <summary>
        /// Gets a value indicating whether rá lehet-e lépni
        /// </summary>
        public override bool IsBlocked => true;
    }
}
