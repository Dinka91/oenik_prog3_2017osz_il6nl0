﻿namespace SubmarineBattle
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    public enum MapEnum
    {
        Lvl01, Lvl02, Lvl03
    }

    /// <summary>
    /// A játéktér modellje, összefogja a pályaelemeket
    /// </summary>
    public class MapModel : Bindable
    {
        /// <summary>
        /// csempe szélessége
        /// </summary>
        public const int TILEW = 40;
        /// <summary>
        /// csempe magassága
        /// </summary>
        public const int TILEH = 40;

        private EmptyTile[,] map;
        private PlayerModel playerA;
        private PlayerModel playerB;

        /// <summary>
        /// Gets or sets melyik pálya
        /// </summary>
        public MapEnum Level { get; set; }

        /// <summary>
        /// Gets Mapmodell példányt
        /// </summary>
        public MapModel Self
        {
            get
            {
                return this;
            }
        }

        /// <summary>
        /// Gets or sets Pályaelemeket tartalmazó tömböt
        /// </summary>
        public EmptyTile[,] Map
        {
            get
            {
                return this.map;
            }

            set
            {
                this.map = value;
                this.OPC("Self");
            }
        }

        /// <summary>
        /// Gets or sets playerA referenciát
        /// </summary>
        public PlayerModel PlayerA { get => this.playerA; set => this.playerA = value; }

        /// <summary>
        /// Gets or sets playerB referenciát
        /// </summary>
        public PlayerModel PlayerB { get => this.playerB; set => this.playerB = value; }
    }
}
