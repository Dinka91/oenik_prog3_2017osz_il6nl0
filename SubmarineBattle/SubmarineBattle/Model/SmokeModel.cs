﻿namespace SubmarineBattle
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Füst pályaelem
    /// </summary>
    public class SmokeModel : EmptyTile
    {
        public SmokeModel(PlayerModel m)
        {
            this.Owner = m;
            this.Timer = 2;
        }

        /// <summary>
        /// Gets a value indicating whether rá lehet-e lépni a mezőre
        /// </summary>
        public override bool IsBlocked => true;

        /// <summary>
        /// Gets visszaadja a hátteret
        /// </summary>
        public override Brush GetBackGround => this.GetBrush("exp2.png");

        /// <summary>
        /// Gets or sets visszaadja a tulajdonos játékost
        /// </summary>
        public PlayerModel Owner { get; set; }

        public int Timer { get; set; }
    }
}
