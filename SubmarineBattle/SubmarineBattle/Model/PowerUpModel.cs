﻿namespace SubmarineBattle
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Fejlesztő pályaelem
    /// </summary>
    public class PowerUpModel : EmptyTile
    {
        /// <summary>
        /// Gets a value indicating whether rá lehet-e lépni a mezőre.
        /// </summary>
        public override bool IsBlocked => false;
        
        /// <summary>
        /// Gets Background
        /// </summary>
        public override Brush GetBackGround => this.GetBrush("powerup.png");
    }
}
