﻿namespace SubmarineBattle
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Akna pályaelem
    /// </summary>
    public class MineModel : EmptyTile
    {
        public MineModel(PlayerModel m)
        {
            this.Owner = m;
            this.DetonationTime = 5;
        }

        /// <summary>
        /// Gets a value indicating whether rá lehet-e lépni
        /// </summary>
        public override bool IsBlocked => true;

        /// <summary>
        /// Gets hátteret
        /// </summary>
        public override Brush GetBackGround => this.GetBrush("mine.png");

        /// <summary>
        /// Gets or sets tulajdonos játékost
        /// </summary>
        public PlayerModel Owner { get; set; }

        /// <summary>
        /// Gets or sets robbanásig hátralévő időt
        /// </summary>
        public int DetonationTime { get; set; }
    }
}
