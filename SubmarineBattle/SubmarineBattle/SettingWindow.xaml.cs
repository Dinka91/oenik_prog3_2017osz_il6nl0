﻿namespace SubmarineBattle
#pragma warning restore SA1652 // Enable XML documentation output
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;

    /// <summary>
    /// SettingWindow.xaml codebehind fájl
    /// </summary>
    public partial class SettingWindow : Window
    {
        private MapModel modell;

       public SettingWindow(MapModel modell)
       {
            this.InitializeComponent();
            this.modell = modell;
            this.tbo_nameA.Text = "Játékos 1";
            this.tbo_nameB.Text = "Játékos2";
            ImageSourceConverter conv = new ImageSourceConverter();
            string path = BusinessLogic.GetPath();
            this.rb_a1.Content = new Image() { Source = conv.ConvertFrom(path + "fast.png") as ImageSource };
            this.rb_a2.Content = new Image() { Source = conv.ConvertFrom(path + "norm.png") as ImageSource };
            this.rb_a3.Content = new Image() { Source = conv.ConvertFrom(path + "slow.png") as ImageSource };
            this.rb_b1.Content = new Image() { Source = conv.ConvertFrom(path + "fast.png") as ImageSource };
            this.rb_b2.Content = new Image() { Source = conv.ConvertFrom(path + "norm.png") as ImageSource };
            this.rb_b3.Content = new Image() { Source = conv.ConvertFrom(path + "slow.png") as ImageSource };
            this.rb_m1.Content = "LEVEL01";
            this.rb_m2.Content = "LEVEL02";
            this.rb_m3.Content = "LEVEL03";
            switch (modell.PlayerA.Type)
            {
                case PlayerTypeEnum.Fast: this.rb_a1.IsChecked = true;
                    break;
                case PlayerTypeEnum.Normal: this.rb_a2.IsChecked = true;
                    break;
                case PlayerTypeEnum.Slow: this.rb_a3.IsChecked = true;
                    break;
                default: this.rb_a2.IsChecked = true;
                    break;
            }

            switch (modell.PlayerB.Type)
            {
                case PlayerTypeEnum.Fast: this.rb_b1.IsChecked = true;
                    break;
                case PlayerTypeEnum.Normal: this.rb_b2.IsChecked = true;
                    break;
                case PlayerTypeEnum.Slow: this.rb_b3.IsChecked = true;
                    break;
                default: this.rb_b2.IsChecked = true;
                    break;
            }

            switch (modell.Level)
            {
                case MapEnum.Lvl01: this.rb_m1.IsChecked = true;
                    break;
                case MapEnum.Lvl02: this.rb_m2.IsChecked = true;
                    break;
                case MapEnum.Lvl03: this.rb_m3.IsChecked = true;
                    break;
                default: this.rb_m1.IsChecked = true;
                    break;
            }
        }

        private void Button_Mentes_Click(object sender, RoutedEventArgs e)
        {
            this.modell.PlayerA.Name = this.tbo_nameA.Text;
            this.modell.PlayerB.Name = this.tbo_nameB.Text;

            PlayerTypeEnum a = PlayerTypeEnum.Normal;
            if (this.rb_a1.IsChecked != null && this.rb_a1.IsChecked.Value)
            {
                a = PlayerTypeEnum.Fast;
            }

            if (this.rb_a3.IsChecked != null && this.rb_a3.IsChecked.Value)
            {
                a = PlayerTypeEnum.Slow;
            }

            PlayerTypeEnum b = PlayerTypeEnum.Normal;
            if (this.rb_b1.IsChecked != null && this.rb_b1.IsChecked.Value)
            {
                b = PlayerTypeEnum.Fast;
            }

            if (this.rb_b3.IsChecked != null && this.rb_b3.IsChecked.Value)
            {
                b = PlayerTypeEnum.Slow;
            }

            this.modell.PlayerA.Type = a;
            this.modell.PlayerB.Type = b;

            if (this.rb_m3.IsChecked != null && this.rb_m3.IsChecked == true)
            {
                this.modell.Level = MapEnum.Lvl03;
            }
            else if (this.rb_m2.IsChecked != null && this.rb_m2.IsChecked == true)
            {
                this.modell.Level = MapEnum.Lvl02;
            }
            else
            {
                this.modell.Level = MapEnum.Lvl01;
            }

            this.DialogResult = true;
            this.Close();
        }

        private void Button_Megse_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
