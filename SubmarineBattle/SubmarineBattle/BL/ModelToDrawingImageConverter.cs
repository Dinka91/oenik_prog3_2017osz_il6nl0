﻿namespace SubmarineBattle
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;

    internal class ModelToDrawingImageConverter : IValueConverter
    {
        /// <summary>
        /// Modelt Drawinggá Converter
        /// </summary>
        /// <param name="value">Mapmodel</param>
        /// <param name="targetType">Üresen hagyható</param>
        /// <param name="parameter">Üresen hagyható ez is </param>
        /// <param name="culture">Üresen hagyható meg ez is</param>
        /// <returns>DrawingImage></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            MapModel model = (MapModel)value;
            if (model == null)
            {
                return null;
            }

            EmptyTile[,] map = model.Map;

            DrawingGroup dg = new DrawingGroup();

            for (int x = 0; x < map.GetLength(0); x++)
            {
                for (int y = 0; y < map.GetLength(1); y++)
                {
                    Rect position = new Rect(x * MapModel.TILEW, y * MapModel.TILEH, MapModel.TILEW, MapModel.TILEH);
                    RectangleGeometry tile = new RectangleGeometry(position);
                    dg.Children.Add(new GeometryDrawing(map[x, y].GetBackGround, null, tile));
                }
            }

            DrawingImage img = new DrawingImage();
            img.Drawing = dg;
            return img;
        }

        /// <summary>
        /// Nem csinál semmit
        /// </summary>
        /// <param name="value">Object</param>
        /// <param name="targetType">Bármi</param>
        /// <param name="parameter">Bármi ez is</param>
        /// <param name="culture">Bármi lehet</param>
        /// <returns>Bindig.DoNothing</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
