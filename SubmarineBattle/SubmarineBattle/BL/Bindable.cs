﻿namespace SubmarineBattle
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// OPCt megvalósító osztály
    /// </summary>
    public class Bindable : INotifyPropertyChanged
    {
        /// <summary>
        /// OPC eseménye
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// OPChez meghívandó metódus
        /// </summary>
        /// <param name="s">Semmi vagy, frissíteni kívánt név</param>
        public void OPC([CallerMemberName] string s = "")
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(s));
        }
    }
}
