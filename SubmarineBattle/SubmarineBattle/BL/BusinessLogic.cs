﻿namespace SubmarineBattle
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Üzleti logikát tartalmazó osztály
    /// </summary>
    public class BusinessLogic
    {
        private static Random rnd = new Random();
        private ScoreModel scoreModel;

        private int sizeX;
        private int sizeY;

        public BusinessLogic(MapEnum map)
        {
            this.Modell = new MapModel();
            this.scoreModel = new ScoreModel();
            this.InitMap(map);
        }

        /// <summary>
        /// Aknatelepítés esetén tüzel
        /// </summary>
        public event EventHandler MinePlantedEvent;

        /// <summary>
        /// Gets,Sets, Property a BL által használt Mapmodell-hez.
        /// </summary>
        public MapModel Modell { get; private set; }

        /// <summary>
        /// Gets,Sets a BL ScoreModel példányát
        /// </summary>
        public ScoreModel ScoreModel { get => this.scoreModel; private set => this.scoreModel = value; }

        /// <summary>
        /// abszolút elérési út string generálása a Resources mappára
        /// </summary>
        /// <returns>Resources mappa abszolút elérési útja</returns>
        public static string GetPath()
        {
            string[] s = Environment.CurrentDirectory.Split('\\');
            string path = string.Empty;
            for (int i = 0; i < s.Length - 2; i++)
            {
                path += s[i] + "\\";
            }

            path += "Resources\\";
            return path;
        }

        /// <summary>
        /// Mozgás előkészítése, széválogatása, enumról számmá alakítása
        /// </summary>
        /// <param name="playerA">playerA merre megy</param>
        /// <param name="playerB">playerB merre megy</param>
        /// <param name="isplayerAMinePlanted">playerA akar-e bombát lerakni</param>
        /// <param name="isplayerBMinePlanted">playerB akar-e bombát lerakni</param>
        /// /// <param name="counter">sebesség meghatározó paraméter</param>
        public void Move(MoveEnum playerA, MoveEnum playerB, bool isplayerAMinePlanted, bool isplayerBMinePlanted, int counter)
        {
            Point pozA = new Point(0, 0);
            Point pozB = new Point(0, 0);
            for (int x = 0; x < this.Modell.Map.GetLength(0); x++)
            {
                for (int y = 0; y < this.Modell.Map.GetLength(1); y++)
                {
                    if (this.Modell.Map[x, y] == this.Modell.PlayerA)
                    {
                        pozA = new Point(x, y);
                    }

                    if (this.Modell.Map[x, y] == this.Modell.PlayerB)
                    {
                        pozB = new Point(x, y);
                    }
                }
            }

            if (this.Modell.PlayerA.Type == PlayerTypeEnum.Slow && counter % 4 != 0)
            {
            }
            else if (this.Modell.PlayerA.Type == PlayerTypeEnum.Normal && counter % 2 != 0)
            {
            }
            else
            {
                switch (playerA)
                {
                    case MoveEnum.Stay: break;
                    case MoveEnum.Up:
                        this.ActualMove(pozA, 0, -1, isplayerAMinePlanted);
                        break;
                    case MoveEnum.Down:
                        this.ActualMove(pozA, 0, 1, isplayerAMinePlanted);
                        break;
                    case MoveEnum.Left:
                        this.ActualMove(pozA, -1, 0, isplayerAMinePlanted);
                        break;
                    case MoveEnum.Rigth:
                        this.ActualMove(pozA, 1, 0, isplayerAMinePlanted);
                        break;
                    default:
                        break;
                }
            }

            switch (playerB)
            {
                case MoveEnum.Stay: break;
                case MoveEnum.Up:
                    this.ActualMove(pozB, 0, -1, isplayerBMinePlanted);
                    break;
                case MoveEnum.Down:
                    this.ActualMove(pozB, 0, 1, isplayerBMinePlanted);
                    break;
                case MoveEnum.Left:
                    this.ActualMove(pozB, -1, 0, isplayerBMinePlanted);
                    break;
                case MoveEnum.Rigth:
                    this.ActualMove(pozB, 1, 0, isplayerBMinePlanted);
                    break;
                default:
                    break;
            }

            this.Modell.Map = this.Modell.Map;
        }

        /// <summary>
        /// Végigmegy a tömbön, kezeli az akna, robbanás füst objektumokat, ellenőrzi a játék végét. Visszaadja a győztes nevét vagy null-t
        /// </summary>
        /// <returns>Győztes neve</returns>
        public string MineTimerChecker()
        {
            for (int x = 0; x < this.Modell.Map.GetLength(0); x++)
            {
                for (int y = 0; y < this.Modell.Map.GetLength(1); y++)
                {
                    if (this.Modell.Map[x, y] is MineModel)
                    {
                        MineModel m = this.Modell.Map[x, y] as MineModel;
                        if (m == null)
                        {
                            throw new ArgumentException("ISMERETLEN HIBA");
                        }

                        m.DetonationTime--;
                        if (m.DetonationTime < 1)
                        {
                            m.Owner.BombCount--;
                            for (int i = x - 1; i < x + 2; i++)
                            {
                                for (int j = y - 1; j < y + 2; j++)
                                {
                                    if (this.Modell.Map[i, j] is FixWall)
                                    {
                                    }
                                    else if (this.Modell.Map[i, j] is PlayerModel)
                                    {
                                        this.Modell.Map[i, j] = new ExplosionModel(m.Owner);
                                        return (this.Modell.Map[i, j] as ExplosionModel).Owner.Name;
                                    }
                                    else
                                    {
                                        this.Modell.Map[i, j] = new ExplosionModel(m.Owner);
                                    }
                                }
                            }
                        }
                    }
                    else if (this.Modell.Map[x, y] is ExplosionModel)
                    {
                        ExplosionModel m = this.Modell.Map[x, y] as ExplosionModel;
                        if (m == null)
                        {
                            throw new ArgumentException("ISMERETLEN HIBA");
                        }

                        m.Timer--;
                        if (m.Timer == 0)
                        {
                            this.Modell.Map[x, y] = new SmokeModel(m.Owner);
                        }
                    }
                    else if (this.Modell.Map[x, y] is SmokeModel)
                    {
                        SmokeModel m = this.Modell.Map[x, y] as SmokeModel;
                        if (m == null)
                        {
                            throw new ArgumentException("ISMERETLEN HIBA");
                        }

                        m.Timer--;
                        if (m.Timer == 0)
                        {
                            if (rnd.Next(10001) % 1000 == 0)
                            {
                                this.Modell.Map[x, y] = new PowerUpModel();
                            }
                            else
                            {
                                this.Modell.Map[x, y] = new EmptyTile();
                            }
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Kör vége esetén eltárolja az eredményt
        /// </summary>
        /// <param name="nev">Nyertes neve</param>
        /// <param name="ido">Eltelt idő</param>
        public void AddNewResult(string nev, TimeSpan ido)
        {
            this.scoreModel.ScoreDict.Add(new KeyValuePair<string, TimeSpan>(nev, ido));
        }

        /// <summary>
        /// Bezárás előtt kiírja a top5 eredményt fájlba
        /// </summary>
        public void FileLogger()
        {
            var myList = this.scoreModel.ScoreDict.ToList();
            myList.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));
            string s = string.Empty;
            for (int i = 0; i < 5; i++)
            {
                s += myList[i].Key + "\t" + myList[i].Value.ToString("mm\\:ss");
                if (i < 4)
                {
                    s += Environment.NewLine;
                }
            }

            File.WriteAllText(BusinessLogic.GetPath() + "Scoreboard.txt", s);
        }

        private void InitMap(MapEnum map)
        {
            string filename = BusinessLogic.GetPath();
            switch (map)
            {
                case MapEnum.Lvl01: filename += "01.lvl";
                    break;
                case MapEnum.Lvl02: filename += "02.lvl";
                    break;
                case MapEnum.Lvl03: filename += "03.lvl";
                    break;
                default: filename += "01.lvl";
                    break;
            }

            if (!File.Exists(filename))
            {
                throw new ArgumentException("ISMERETLEN HIBA");
            }

            string[] s = File.ReadAllLines(filename);
            this.sizeY = s.Length;  // 10
            this.sizeX = s[0].Length; // 15
            this.Modell.Map = new EmptyTile[this.sizeX, this.sizeY];
            for (int y = 0; y < this.sizeY; y++)
            {
                for (int x = 0; x < this.sizeX; x++)
                {
                    if (s[y][x] == 'x')
                    {
                        this.Modell.Map[x, y] = new FixWall();
                    }
                    else if (s[y][x] == 'f')
                    {
                        this.Modell.Map[x, y] = new DestroyableWall();
                    }
                    else if (s[y][x] == 'a')
                    {
                        this.Modell.PlayerA = new PlayerModel();
                        this.Modell.Map[x, y] = this.Modell.PlayerA;
                    }
                    else if (s[y][x] == 'b')
                    {
                        this.Modell.PlayerB = new PlayerModel();
                        this.Modell.Map[x, y] = this.Modell.PlayerB;
                    }
                    else
                    {
                        this.Modell.Map[x, y] = new EmptyTile();
                    }
                }
            }
        }

        private void ActualMove(Point p, int directionX, int directionY, bool isBombPlanted)
        {
            int x = (int)p.X;
            int y = (int)p.Y;

            if (y + directionY < 0 || y + directionY > this.sizeY - 1 || x + directionX < 0 || x + directionX > this.sizeX - 1)
            {
                return;
            }

            if (this.Modell.Map[x + directionX, y + directionY].IsBlocked)
            {
                return;
            }

            if (this.Modell.Map[x + directionX, y + directionY] is PowerUpModel)
            {
                (this.Modell.Map[x, y] as PlayerModel).IsPoweredUp = true;
            }

            this.Modell.Map[x + directionX, y + directionY] = this.Modell.Map[x, y];

            if (isBombPlanted)
            {
                int max;
                PlayerModel player = this.Modell.Map[x, y] as PlayerModel;
                if (player == null)
                {
                    throw new ArgumentException("ISMERETLEN HIBA");
                }

                if (player.Type == PlayerTypeEnum.Slow)
                {
                    max = 2;
                }
                else
                {
                    max = 1;
                }

                if (player.IsPoweredUp)
                {
                    max++;
                }

                if (player.BombCount < max)
                {
                    this.Modell.Map[x, y] = new MineModel(player);
                    player.BombCount++;
                }
                else
                {
                    this.Modell.Map[x, y] = new EmptyTile();
                }

                this.MinePlantedEvent?.Invoke(null, null);
            }
            else
            {
                this.Modell.Map[x, y] = new EmptyTile();
            }
        }
    }
}